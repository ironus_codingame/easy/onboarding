/**
 * Test case for Solution class.
 */
package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import main.Player;

/**
 * Test case for Solution class.
 * @author Ironus: konrad.zierek@gmail.com
 */
class PlayerTest {
  @Test
  void testSingleEnemyPositiveDistance() {
    final String enemy1 = "Rock";
    final int dist1 = 70;
    
    String result = Player.getClosestEnemy(enemy1, dist1);
    assertEquals(enemy1, result);
  }

  @Test
  void testSingleEnemyTestNegativeDistance() {
    final String enemy1 = "HotDroid";
    final int dist1 = -70;

    String result = Player.getClosestEnemy(enemy1, dist1);
    assertEquals(enemy1, result);
  }
  
  @Test
  void testTwoEnemies1BothPositive() {
    final String enemy1 = "HardHat";
    final int dist1 = 70;
    
    final String enemy2 = "Bolt";
    final int dist2 = 70;
    
    String result = Player.getClosestEnemy(enemy1, dist1, enemy2, dist2);
    assertEquals(enemy2, result);
  }
  
  @Test
  void testTwoEnemies2BothNegative() {
    final String enemy1 = "Sectoid";
    final int dist1 = -56;
    
    final String enemy2 = "HardHat";
    final int dist2 = -60;
    
    String result = Player.getClosestEnemy(enemy1, dist1, enemy2, dist2);
    assertEquals(enemy1, result);
  }
}
