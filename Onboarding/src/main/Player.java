/**
 * Onboarding solution.
 */

package main;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class with the solution.
 * 
 * @author Ironus: konrad.zierek@gmail.com
 *
 */
public class Player {
  private static final Logger logger = Logger.getLogger(Player.class.getName());
  
  private Player() {}

  /**
   * Return the closest enemy to the player if only one supplied.
   * @param enemy1 - first enemy name
   * @param dist1 - first enemy distance
   * @return - name of the closest enemy
   */
  public static String getClosestEnemy(String enemy1, int dist1) {
    if (dist1 < 0) {
      StringBuilder builder = new StringBuilder().append("Distance ").append(dist1).append(" is negative");
      String msg = builder.toString();
      logger.log(Level.INFO, msg);
    }

    return enemy1;
  }
  
  /**
   * Returns the closest enemy to the player.
   * @param enemy1 - first enemy name
   * @param dist1 - first enemy distance
   * @param enemy2 - second enemy name
   * @param dist2 - second enemy distance
   * @return - name of the closest enemy
   */
  public static String getClosestEnemy(String enemy1, int dist1, String enemy2, int dist2) {
    return (Math.abs(dist1) < Math.abs(dist2)) ? enemy1 : enemy2;
  }
}
